## Tests for Database

1. What to mock?
2. Risks with custom queries
3. Risks with in-memory database
4. Avoid pitfalls

### Three Keys to write an effective integration test for database layer

A) Choose the right interactions to test

For example `interface StudentRepository extends CrudRepository<Student, Long>`

* custom method name `Student getStudentByName(String name)`

* custom JPQL query `@Query(value "Select s from StudentTypo s") List<Student> getAllStudentsCustom()`

* native SQL `@Query(value "Select * from StudentTypo") List<Student> getAllStudentsNative()`

**custom method name AND custom JPQL would be validated at startup** so there is a little risk involved. But
**native queries** are not validated at startup so native queries are primary candidates for integration tests.

B) Make your tests fast by using test slices

Running integration tests in spring can be achieved by adding spring boot test annotation, 
which loads full spring context. However, this can lead to loading unnecessary parts of your application. 
For exampled, you might want to test database interactions(ex. @DataJpaTest) or you are only interested in web layer(ex. @MockMvcTest).

C) In-memory database != production database

Embedded Database 

1. H2/HSQL

2. Compatibility mode (MySQL and Postgres)

(Risk: not in sync with database we are using in production)

**@Testcontainers**

1. Throw away database instances in Docker

2. Simple setup

3. Define database type and version

4. Each time fresh instance










