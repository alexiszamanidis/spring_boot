package testing;

import org.assertj.core.api.BDDAssertions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;

@DataJpaTest
public class StudentRepositoryTest {

	@Autowired
	private StudentRepository studentRepository;

	// TestEntityManager is a spring Bean.
	// with TestEntityManager we can use persistFlushFind() method
	// to force synchronization by inserting object to the database and
	// detaching it
	@Autowired
	TestEntityManager testEntityManager;

	void testGetStudentByName_returnsStudentDetails() {
		// given
		Student savedStudent = testEntityManager.persistFlushFind(studentRepository.save(new Student(null, "alex")));

		// when
		Student student = studentRepository.getStudentByName("alex");

		// then
		BDDAssertions.then(savedStudent.getId()).isNotNull();
		BDDAssertions.then(student.getName()).isEqualTo(student.getName());
	}

}
