# Spring Boot

Just learning Spring Boot

**Table of Contents**

* [Associations](https://gitlab.com/alexiszamanidis/spring_boot/-/tree/master/associations)
* [Lombok](https://gitlab.com/alexiszamanidis/spring_boot/-/tree/master/lombok)
* [Pagination and Sorting](https://gitlab.com/alexiszamanidis/spring_boot/-/tree/master/pagination_and_sorting)
* [Search](https://gitlab.com/alexiszamanidis/spring_boot/-/tree/master/search)
* [Security](https://gitlab.com/alexiszamanidis/spring_boot/-/tree/master/security)
* [Testing](https://gitlab.com/alexiszamanidis/spring_boot/-/tree/master/testing)
* [Thymeleaf](https://gitlab.com/alexiszamanidis/spring_boot/-/tree/master/thymeleaf)

## ObjectAid: Automatically generate UML diagrams from Java Code

The [ObjectAid](https://www.objectaid.com/home) UML Explorer is an agile and lightweight code visualization tool for the Eclipse IDE. 
It uses the UML notation to show a graphical representation of existing Java code that is as accurate and up-to-date as your text 
editor, while being very easy to use.

## Swagger

Nowadays, front-end and back-end components often separate a web application. Usually, we expose APIs as a back-end component for the 
front-end component or third-party app integrations.

In such a scenario, it is essential to have proper specifications for the back-end APIs. At the same time, the API documentation 
should be informative, readable, and easy to follow.

Moreover, reference documentation should simultaneously describe every change in the API. Accomplishing this manually is a tedious exercise, 
so automation of the process was inevitable.

**Tools to:**

* Develop APIs
* Interact with APIs
* Document APIs

## SonarLint

[SonarLint](https://www.sonarqube.org/sonarlint/) is a free IDE extension that finds bugs and vulnerabilities while you code. 

SonarLint is available for:

* Eclipse
* IntelliJ
* Visual Studio
* VS Code