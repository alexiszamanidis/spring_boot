package swagger.swagger.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = "/test")
public class TestController {

    @GetMapping
    public String test() {
        return "Test";
    }

    @GetMapping(value = "/{id}")
    public String test2(@PathVariable Integer id) {
        return String.format("Test %d", id);
    }

}
