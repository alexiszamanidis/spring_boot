## Docker

Simple Dockerfile in Spring Boot.

First of all, you should add a finalName in pom.xml file, then add the finalName.jar into your Dockerfile.

```
mvn clean install
docker build -t <image-name>:<tag-name> .
docker run -it -p <local-port>:8080 <image-name>:<tag-name>
```

#### Dependencies

* Spring Web
* DevTools
