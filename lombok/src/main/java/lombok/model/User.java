package lombok.model;

import lombok.NoArgsConstructor;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Setter;

import lombok.Getter;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class User {

	private int id;
	private String firstName;
	private String lastName;
	
}
