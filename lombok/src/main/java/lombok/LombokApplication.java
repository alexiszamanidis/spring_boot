package lombok;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import lombok.model.User;

@SpringBootApplication
public class LombokApplication {

	public static void main(String[] args) {
		SpringApplication.run(LombokApplication.class, args);
		User user = new User(1, "alexis", "zamanidis");
		System.out.println(user.getId() + " " + user.getFirstName() + " " + user.getLastName());
	}

}
