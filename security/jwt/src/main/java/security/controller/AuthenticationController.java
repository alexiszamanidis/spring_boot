package security.controller;

import java.util.HashMap;
import java.util.Map;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import security.exception.ErrorServiceException;
import security.model.Authority;
import security.model.Role;
import security.model.User;
import security.request.SignInRequest;
import security.security.MyUserDetails;
import security.security.jwt.JwtResponse;
import security.security.jwt.JwtUtils;
import security.service.AuthorityService;
import security.service.RoleService;
import security.service.UserService;
import security.service.ValidationErrorService;

@CrossOrigin(origins = "*")
@RestController
@RequestMapping(value = "/auth")
public class AuthenticationController {

    @Autowired
    private AuthenticationManager authenticationManager;

    @Autowired
    private BCryptPasswordEncoder encoder;

    @Autowired
    private JwtUtils jwtUtils;

    @Autowired
    private UserService userService;

    @Autowired
    private RoleService roleService;

    @Autowired
    private AuthorityService authorityService;

    @Autowired
    private ValidationErrorService validationErrorService;

    @PostMapping(value = "/signUp", produces = "application/json")
    public ResponseEntity<?> signUp(@Valid @RequestBody User userInfo, BindingResult result)
            throws ErrorServiceException {
        // validate the input, exception will be thrown
        validationErrorService.validationService(result);

        // check if the username already exists
        if (userService.existsByUserName(userInfo.getUserName()) == true) {
            Map<String, String> errors = new HashMap<String, String>();
            errors.put("error", "Username already exists");
            throw new ErrorServiceException(errors);
        }
        // check if the email already exists
        if (userService.existsByEmail(userInfo.getEmail()) == true) {
            Map<String, String> errors = new HashMap<String, String>();
            errors.put("error", "Email already exists");
            throw new ErrorServiceException(errors);
        }

        // create new user object
        User newUser = new User(userInfo.getUserName(), userInfo.getEmail(), encoder.encode(userInfo.getPassword()));
        // set the roles to user
        Role role = roleService.findRoleByName("ROLE_USER");
        System.out.println("role: " + role.toString());
        newUser.getRoles().add(role);
        // set the authorities to user
        String[] authorities = { "AUTHORITY_UPDATE", "AUTHORITY_READ" };
        for (String auth : authorities) {
            Authority authority = authorityService.findAuthorityByName(auth);
            newUser.getAuthorities().add(authority);
        }

        return new ResponseEntity<>(userService.saveOrUpdateUser(newUser), HttpStatus.CREATED);
    }

    @PostMapping(value = "/signIn", produces = "application/json")
    public ResponseEntity<?> signIn(@Valid @RequestBody SignInRequest credentials, BindingResult result)
            throws ErrorServiceException {
        // validate the input, exception will be thrown
        validationErrorService.validationService(result);

        // get the credentials
        User user = userService.findUserByUserNameOrEmail(credentials.getUserNameOrEmail());
        if (user == null) {
            Map<String, String> errors = new HashMap<String, String>();
            errors.put("error", "Bad credentials");
            throw new ErrorServiceException(HttpStatus.UNAUTHORIZED, errors);
        }
        String password = credentials.getPassword();

        // comparing the password submitted against the one loaded by the
        // UserDetailsService
        // if the do not match with one user at the database return 401-unauthorized
        Authentication authentication = authenticationManager
                .authenticate(new UsernamePasswordAuthenticationToken(String.valueOf(user.getId()), password));

        // set the details of the security context
        SecurityContextHolder.getContext().setAuthentication(authentication);

        // generate the JWT token using the authentication
        String jwt = jwtUtils.generateJwtToken(authentication);
        // the user that is currently authenticated
        MyUserDetails userDetails = (MyUserDetails) authentication.getPrincipal();
        // create a new (custom) JWT reponse
        JwtResponse jwtResponse = new JwtResponse(jwt, user.getId(), userDetails.getAuthorities());

        return new ResponseEntity<>(jwtResponse, HttpStatus.OK);
    }
}
