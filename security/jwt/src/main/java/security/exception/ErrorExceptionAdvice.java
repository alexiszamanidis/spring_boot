package security.exception;

import java.util.HashMap;
import java.util.Map;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;

@ControllerAdvice
public class ErrorExceptionAdvice {

    @ExceptionHandler(ErrorServiceException.class)
    public ResponseEntity<Error> notFoundException(ErrorServiceException ex, WebRequest request) {
        Error error = new Error(ex.getStatus().value(), ex.getErrors(), request.getDescription(false));
        return new ResponseEntity<Error>(error, ex.getStatus());
    }

    @ExceptionHandler(HttpMessageNotReadableException.class)
    public ResponseEntity<Error> customHttpMessageNotReadableException(Exception e, WebRequest request) {
        Map<String, String> errors = new HashMap<String, String>();
        errors.put("error", e.getMessage());
        Error error = new Error(HttpStatus.BAD_REQUEST.value(), errors, request.getDescription(false));
        return new ResponseEntity<Error>(error, HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(AccessDeniedException.class)
    public ResponseEntity<Error> customAccessDeniedException(Exception e, WebRequest request) {
        Map<String, String> errors = new HashMap<String, String>();
        errors.put("error", e.getMessage());
        Error error = new Error(HttpStatus.UNAUTHORIZED.value(), errors, request.getDescription(false));
        return new ResponseEntity<Error>(error, HttpStatus.UNAUTHORIZED);
    }

    @ExceptionHandler(Exception.class)
    public ResponseEntity<Error> globalException(Exception e, WebRequest request) {
        Map<String, String> errors = new HashMap<String, String>();
        errors.put("error", e.getMessage());
        Error error = new Error(HttpStatus.INTERNAL_SERVER_ERROR.value(), errors, request.getDescription(false));
        return new ResponseEntity<Error>(error, HttpStatus.INTERNAL_SERVER_ERROR);
    }

}
