package security.exception;

import java.util.Map;

import org.springframework.http.HttpStatus;

import lombok.Getter;
import lombok.Setter;

@SuppressWarnings("serial")
@Getter
@Setter
public class ErrorServiceException extends Exception {

    private HttpStatus status = HttpStatus.BAD_REQUEST;
    private Map<String, String> errors;

    public ErrorServiceException(Map<String, String> errors) {
        this.errors = errors;
    }

    public ErrorServiceException(HttpStatus status, Map<String, String> errors) {
        this.status = status;
        this.errors = errors;
    }

    public ErrorServiceException(HttpStatus status, Map<String, String> errors, String message) {
        super(message);
        this.status = status;
        this.errors = errors;
    }

}
