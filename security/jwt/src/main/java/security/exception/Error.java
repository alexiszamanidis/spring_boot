package security.exception;

import java.util.Date;
import java.util.Map;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class Error {

    private int status;

    private Map<String, String> errors;

    private String url;

    private Date errorDate = new Date();

    public Error(int status, Map<String, String> errors, String url) {
        this.status = status;
        this.errors = errors;
        this.url = url;
    }

}
