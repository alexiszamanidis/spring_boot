package security.security.jwt;

import java.util.Date;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Component;

import io.jsonwebtoken.ExpiredJwtException;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.MalformedJwtException;
import io.jsonwebtoken.SignatureAlgorithm;
import io.jsonwebtoken.SignatureException;
import io.jsonwebtoken.UnsupportedJwtException;
import security.exception.ErrorServiceException;
import security.model.User;
import security.security.MyUserDetails;
import security.service.UserService;

// This class contains JWT utilities
@Component
public class JwtUtils {

	private static final Logger logger = LoggerFactory.getLogger(JwtUtils.class);

	// get a secret key which is provided inside application properties
	@Value("${security.jwtSecret}")
	private String jwtSecret;

	// get how much time for the token to expire from application properties
	@Value("${security.jwtExpiration}")
	private int jwtExpiration;

	@Autowired
	private UserService userService;

	// generate a JWT from username, date, expiration, secret
	public String generateJwtToken(Authentication authentication) {
		MyUserDetails userPrincipal = (MyUserDetails) authentication.getPrincipal();
		return Jwts.builder()
				// add username to jwt
				.setSubject(String.valueOf(userPrincipal.getUser().getId()))
				// add authorities to jwt
				.claim("authorities", userPrincipal.getAuthorities()).setIssuedAt(new Date())
				.setExpiration(new Date((new Date()).getTime() + jwtExpiration))
				.signWith(SignatureAlgorithm.HS512, jwtSecret).compact();
	}

	// get username from JWT,
	// parse through the valid tokens in order to get the username connected to a
	// valid token
	public Long getIdFromJwtToken(String token) {
		return Long.valueOf(Jwts.parser().setSigningKey(jwtSecret).parseClaimsJws(token).getBody().getSubject());
	}

	// validate a JWT
	public boolean validateJwtToken(String authToken) {
		try {
			// check the validation of the authorization token
			Jwts.parser().setSigningKey(jwtSecret).parseClaimsJws(authToken);
			return true;
		}
		// if something is wrong throw the appropriate exception
		catch (SignatureException e) {
			logger.error("Invalid JWT signature: {}", e.getMessage());
		} catch (MalformedJwtException e) {
			logger.error("Invalid JWT token: {}", e.getMessage());
		} catch (ExpiredJwtException e) {
			logger.error("JWT token is expired: {}", e.getMessage());
		} catch (UnsupportedJwtException e) {
			logger.error("JWT token is unsupported: {}", e.getMessage());
		} catch (IllegalArgumentException e) {
			logger.error("JWT claims string is empty: {}", e.getMessage());
		}

		return false;
	}

	public String parseJwt(HttpServletRequest request) {
		String headerAuth = request.getHeader("Authorization");

		// be sure it is in correct form
		if (headerAuth != null && headerAuth.startsWith("Bearer ")) {
			// remove the "Bearer " in front of the token
			return headerAuth.replace("Bearer ", "");
		}

		return null;
	}

	public String parseJwt(String headerAuth) {
		// be sure it is in correct form
		if (headerAuth != null && headerAuth.startsWith("Bearer ")) {
			// remove the "Bearer " in front of the token
			return headerAuth.replace("Bearer ", "");
		}

		return null;
	}

	public User getUserFromAuthHeaderToken(String authHeaderToken) throws ErrorServiceException {
		// get token
		String token = this.parseJwt(authHeaderToken);
		// get id from JWT token
		Long id = this.getIdFromJwtToken(token);
		// find user by the id that we extract from the JWT token
		User user = userService.findUserById(id);

		return user;
	}

	public Long getIdFromAuthHeaderToken(String authHeaderToken) throws ErrorServiceException {
		// get token
		String token = this.parseJwt(authHeaderToken);
		// get id from JWT token
		Long id = this.getIdFromJwtToken(token);

		return id;
	}

}
