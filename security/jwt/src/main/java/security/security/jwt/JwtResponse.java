package security.security.jwt;

import java.util.Collection;
import java.util.Date;

import org.springframework.security.core.GrantedAuthority;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class JwtResponse {

    private String token;

    private String tokenType = "Bearer";

    private Long id;

    private Collection<? extends GrantedAuthority> authorities;

    private Date tokenCreationDate = new Date();

    public JwtResponse(String token, Long id, Collection<? extends GrantedAuthority> authorities) {
        this.token = token;
        this.id = id;
        this.authorities = authorities;
    }

}
