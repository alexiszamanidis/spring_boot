package security.security;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import security.model.User;
import security.repository.UserRepository;

@Service
public class UserDetailsServiceImpl implements UserDetailsService {

    @Autowired
    private UserRepository userRepository;

    @Override
	public UserDetails loadUserByUsername(String id) throws UsernameNotFoundException {
		Optional<User> user = userRepository.findById(Long.parseLong(id));

		if (!user.isPresent()) {
			throw new UsernameNotFoundException("Could not find the user with ID '" + id + "'");
		}

		return new MyUserDetails(user.get());
	}

}
