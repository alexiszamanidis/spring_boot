package security.security;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.dao.DaoAuthenticationProvider;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

import security.security.jwt.JwtAuthEntryPoint;
import security.security.jwt.JwtAuthTokenFilter;

@SuppressWarnings("deprecation")
@Configuration
@EnableWebSecurity
@EnableGlobalMethodSecurity(prePostEnabled = true)
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {

    @Autowired
    private UserDetailsServiceImpl userDetailsService;

    // exception handler to be chosen
    @Autowired
    private JwtAuthEntryPoint unauthorizedHandler;

    // authenticate the token received
    @Bean
    public JwtAuthTokenFilter authenticationJwtTokenFilter() {
        return new JwtAuthTokenFilter();
    }

    @Bean
    @Override
    public AuthenticationManager authenticationManagerBean() throws Exception {
        return super.authenticationManagerBean();
    }

    // password encoder 'encodes the password'
    @Bean
    public BCryptPasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }
    // password encoder 'does NOT encode the password'
    // @Bean
    // public PasswordEncoder passwordEncoder() {
    // return NoOpPasswordEncoder.getInstance();
    // }

    // Authentication Provider
    // Dependencies: a) UserDetailsService
    // b) passwordEncoder
    @Bean
    public DaoAuthenticationProvider authenticationProvider() {
        DaoAuthenticationProvider authProvider = new DaoAuthenticationProvider();

        authProvider.setUserDetailsService(userDetailsService);
        authProvider.setPasswordEncoder(passwordEncoder());

        return authProvider;
    }

    // Authentication
    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        auth.authenticationProvider(authenticationProvider());
    }

    // Authorisation
    @Override
    protected void configure(HttpSecurity http) throws Exception {
        // disable csrf(Cross-Site Request Forgery)
        http.csrf().disable();

        // set exception handler
        http.exceptionHandling().authenticationEntryPoint(unauthorizedHandler);

        // JWT are stateless, so make sure to use STATELESS authentication
        http.sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS);

        // permit sign-in and sign-up
        http.authorizeRequests().antMatchers("/auth/**").permitAll();

        // permit test controller
        http.authorizeRequests().antMatchers("/test/**").permitAll();

        // all other requests needs to be authenticated (now we permit them all)
        http.authorizeRequests().anyRequest().permitAll();

        // to specify which class will add authorize the token before a request is asked
        http.addFilterBefore(authenticationJwtTokenFilter(), UsernamePasswordAuthenticationFilter.class);
    }

}
