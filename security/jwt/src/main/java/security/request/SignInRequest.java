package security.request;

import javax.validation.constraints.NotBlank;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class SignInRequest {

    @NotBlank(message = "Username/Email is required")
    private String userNameOrEmail;

    @NotBlank(message = "Password is required")
    private String password;

}
