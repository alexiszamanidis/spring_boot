package security.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.MappedSuperclass;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@MappedSuperclass
public abstract class DateAudit {

	@Column(name = "created_date")
	private Date createdDate = new Date();

	@Column(name = "updated_date")
	private Date updatedDate;

}
