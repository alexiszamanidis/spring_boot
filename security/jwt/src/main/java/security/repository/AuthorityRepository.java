package security.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import security.model.Authority;

@Repository
public interface AuthorityRepository extends JpaRepository<Authority, Integer> {

    public Optional<Authority> findByName(String name);

}
