package security.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import security.model.User;

@Repository
public interface UserRepository extends JpaRepository<User, Long> {

    public Optional<User> findById(Long id);

    public User findByUserName(String userName);

    public Boolean existsByUserName(String userName);

    public Boolean existsByEmail(String email);

    @Query("SELECT u FROM User u WHERE userName = ?1 or email = ?1")
    public User findByUserNameOrEmail(String userNameOrEmail);

}
