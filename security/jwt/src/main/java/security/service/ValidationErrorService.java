package security.service;

import java.util.HashMap;
import java.util.Map;

import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;

import security.exception.ErrorServiceException;

@Service
public class ValidationErrorService {

    public ResponseEntity<?> validationService(BindingResult result) throws ErrorServiceException {
        if (!result.hasErrors())
            return null;

        Map<String, String> errors = new HashMap<String, String>();
        // put all errors in a map
        for (FieldError error : result.getFieldErrors()) {
            errors.put(error.getField(), error.getDefaultMessage());
        }
        throw new ErrorServiceException(errors);
    }

}
