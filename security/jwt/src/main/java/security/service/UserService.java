package security.service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import security.exception.ErrorServiceException;
import security.model.User;
import security.repository.UserRepository;

@Service
public class UserService {

	@Autowired
	private UserRepository userRepository;

	public List<User> findAllUsers() {
		return (List<User>) userRepository.findAll();
	}

	public User saveOrUpdateUser(User user) throws ErrorServiceException {
		try {
			return userRepository.save(user);
		} catch (Exception e) {
			Map<String, String> errors = new HashMap<String, String>();
			errors.put("error", "User with Username '" + user.getUserName() + "' or Email '" + user.getEmail()
					+ "' already exists");
			throw new ErrorServiceException(errors);
		}
	}

	public User findUserById(Long id) throws ErrorServiceException {
		Optional<User> user = userRepository.findById(id);

		if (!user.isPresent()) {
			Map<String, String> errors = new HashMap<String, String>();
			errors.put("error", "User with id '" + id + "' does not exist");
			throw new ErrorServiceException(errors);
		}

		return user.get();
	}

	public Boolean existsByUserName(String userName) {
		return userRepository.existsByUserName(userName);
	}

	public Boolean existsByEmail(String email) {
		return userRepository.existsByEmail(email);
	}

	public User findUserByUserNameOrEmail(String userNameOrEmail) throws ErrorServiceException {
		User user = userRepository.findByUserNameOrEmail(userNameOrEmail);

		if (user == null) {
			Map<String, String> errors = new HashMap<String, String>();
			errors.put("error", "User with Username/Email '" + userNameOrEmail + "' does not exist");
			throw new ErrorServiceException(errors);
		}

		return user;
	}

	public User findUserByUserNameOrEmailNoException(String userNameOrEmail) {
		User user = userRepository.findByUserNameOrEmail(userNameOrEmail);

		if (user == null)
			return null;

		return user;
	}

}
