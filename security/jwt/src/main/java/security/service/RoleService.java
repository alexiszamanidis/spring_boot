package security.service;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import security.exception.ErrorServiceException;
import security.model.Role;
import security.repository.RoleRepository;

@Service
public class RoleService {

    @Autowired
    private RoleRepository roleRepository;

    public Role findRoleByName(String name) throws ErrorServiceException {
        try {
            Optional<Role> role = roleRepository.findByName(name);
            return role.get();
        } catch (Exception e) {
            Map<String, String> errors = new HashMap<String, String>();
            errors.put("error", "Role with name '" + name + "' does not exist");
            throw new ErrorServiceException(errors);
        }
    }

}