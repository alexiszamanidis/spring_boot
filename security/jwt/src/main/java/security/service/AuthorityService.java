package security.service;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import security.exception.ErrorServiceException;
import security.model.Authority;
import security.repository.AuthorityRepository;

@Service
public class AuthorityService {

    @Autowired
    private AuthorityRepository authorityRepository;

    public Authority findAuthorityByName(String name) throws ErrorServiceException {
        try {
            Optional<Authority> authority = authorityRepository.findByName(name);
            return authority.get();
        } catch (Exception e) {
            Map<String, String> errors = new HashMap<String, String>();
            errors.put("error", "Authority with name '" + name + "' does not exist");
            throw new ErrorServiceException(errors);
        }
    }

}
