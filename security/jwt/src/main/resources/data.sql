-- insert users
insert into user(id, user_name, email, password, created_date, updated_date) values (1, "alexzam", "alexzam@gmail.com", "$2y$12$5guO2wLry5eDpMnRxe1ZB.lbqQ5RVZ8fEfO3KFJue7qmI/Vj4fYPK", CURRENT_TIMESTAMP, null);
insert into user(id, user_name, email, password, created_date, updated_date) values (2, "johndar", "johndar@gmail.com", "$2y$12$5guO2wLry5eDpMnRxe1ZB.lbqQ5RVZ8fEfO3KFJue7qmI/Vj4fYPK", CURRENT_TIMESTAMP, null);
insert into user(id, user_name, email, password, created_date, updated_date) values (3, "nickntaf", "nickntaf@gmail.com", "$2y$12$5guO2wLry5eDpMnRxe1ZB.lbqQ5RVZ8fEfO3KFJue7qmI/Vj4fYPK", CURRENT_TIMESTAMP, null);
insert into user(id, user_name, email, password, created_date, updated_date) values (4, "vagdem", "vagdem@gmail.com", "$2y$12$5guO2wLry5eDpMnRxe1ZB.lbqQ5RVZ8fEfO3KFJue7qmI/Vj4fYPK", CURRENT_TIMESTAMP, null);
insert into user(id, user_name, email, password, created_date, updated_date) values (5, "stathdem", "stathdem@gmail.com", "$2y$12$5guO2wLry5eDpMnRxe1ZB.lbqQ5RVZ8fEfO3KFJue7qmI/Vj4fYPK", CURRENT_TIMESTAMP, null);

-- insert roles
insert into role(id, name, created_date, updated_date) values (1, "ROLE_USER", CURRENT_TIMESTAMP, null);
insert into role(id, name, created_date, updated_date) values (2, "ROLE_ADMIN", CURRENT_TIMESTAMP, null);
insert into role(id, name, created_date, updated_date) values (3, "ROLE_MODERATOR", CURRENT_TIMESTAMP, null);

-- insert authorities
insert into authority(id, name, created_date, updated_date) values (1, "AUTHORITY_READ", CURRENT_TIMESTAMP, null);
insert into authority(id, name, created_date, updated_date) values (2, "AUTHORITY_WRITE", CURRENT_TIMESTAMP, null);
insert into authority(id, name, created_date, updated_date) values (3, "AUTHORITY_UPDATE", CURRENT_TIMESTAMP, null);
insert into authority(id, name, created_date, updated_date) values (4, "AUTHORITY_DELETE", CURRENT_TIMESTAMP, null);

-- insert user-role
insert into user_role(user_id, role_id) values (1, 1);
insert into user_role(user_id, role_id) values (1, 2);
insert into user_role(user_id, role_id) values (1, 3);

insert into user_role(user_id, role_id) values (2, 1);
insert into user_role(user_id, role_id) values (2, 3);

insert into user_role(user_id, role_id) values (3, 1);
insert into user_role(user_id, role_id) values (3, 3);

insert into user_role(user_id, role_id) values (4, 1);
insert into user_role(user_id, role_id) values (4, 3);

insert into user_role(user_id, role_id) values (5, 1);

-- insert user-authority
insert into user_authority(user_id, authority_id) values (1, 1);
insert into user_authority(user_id, authority_id) values (1, 2);
insert into user_authority(user_id, authority_id) values (1, 3);
insert into user_authority(user_id, authority_id) values (1, 4);

insert into user_authority(user_id, authority_id) values (2, 1);
insert into user_authority(user_id, authority_id) values (2, 2);
insert into user_authority(user_id, authority_id) values (2, 3);

insert into user_authority(user_id, authority_id) values (3, 1);
insert into user_authority(user_id, authority_id) values (3, 2);
insert into user_authority(user_id, authority_id) values (3, 3);

insert into user_authority(user_id, authority_id) values (4, 1);
insert into user_authority(user_id, authority_id) values (4, 2);
insert into user_authority(user_id, authority_id) values (4, 3);

insert into user_authority(user_id, authority_id) values (5, 1);

