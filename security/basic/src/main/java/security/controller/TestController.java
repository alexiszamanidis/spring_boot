package security.controller;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@CrossOrigin(origins = "*")
@RestController
@RequestMapping(value = "/test")
public class TestController {

	@GetMapping("/everyone")
	public String everyone() {
		return "Everyone content";
	}
	
	@GetMapping("/user")
	public String user() {
		return "User Content";
	}
	
	@GetMapping("/moderator")
	public String moderator() {
		return "Moderator Content";
	}

	@GetMapping("/admin")
	public String admin() {
		return "Admin Content";
	}
	
}
