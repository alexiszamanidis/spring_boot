package security.basic;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Set;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import security.model.Authority;
import security.model.Role;
import security.model.User;

@SuppressWarnings("serial")
public class MyUserDetails implements UserDetails {

	private User user;
	
	public MyUserDetails(User user) {
		this.user = user;
	}

	@Override
	public Collection<? extends GrantedAuthority> getAuthorities() {
		List<SimpleGrantedAuthority> authoritiesList = new ArrayList<>();
		
		Set<Role> roles = user.getRoles();
		// add all the roles
		for( Role role : roles ) {
			authoritiesList.add(new SimpleGrantedAuthority(role.getName()));
		}
		Set<Authority> authorities = user.getAuthorities();
		// add all the authorities
		for( Authority authority : authorities ) {
			authoritiesList.add(new SimpleGrantedAuthority(authority.getName()));
		}
		
		return authoritiesList;
	}

	@Override
	public String getPassword() {
		return user.getPassword();
	}

	@Override
	public String getUsername() {
		return user.getUserName();
	}

	@Override
	public boolean isAccountNonExpired() {
		return true;
	}

	@Override
	public boolean isAccountNonLocked() {
		return true;
	}

	@Override
	public boolean isCredentialsNonExpired() {
		return true;
	}

	@Override
	public boolean isEnabled() {
		return true;
	}

}
