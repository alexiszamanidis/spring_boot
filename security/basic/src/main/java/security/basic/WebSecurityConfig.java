package security.basic;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.dao.DaoAuthenticationProvider;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.password.NoOpPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

@Configuration
@EnableWebSecurity
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {

	@Autowired
	private UserDetailsServiceImpl userDetailsService;
	
	
	
	// password encoder 'encodes the password'
//		@Bean
//		public BCryptPasswordEncoder passwordEncoder() {
//			return new BCryptPasswordEncoder();
//		}
	// password encoder 'does NOT encode the password'
	@Bean
	public PasswordEncoder passwordEncoder() {
		return NoOpPasswordEncoder.getInstance();
	}
	
	// Authentication Provider
	// Dependencies: a) UserDetailsService
	//               b) passwordEncoder
	@Bean
	public DaoAuthenticationProvider authenticationProvider() {
		DaoAuthenticationProvider authProvider = new DaoAuthenticationProvider();
		
		authProvider.setUserDetailsService(userDetailsService);
		authProvider.setPasswordEncoder(passwordEncoder());
		
		return authProvider;
	}
	
	// Authentication
	@Override
	protected void configure(AuthenticationManagerBuilder auth) throws Exception {
		auth.authenticationProvider(authenticationProvider());
	}
	
	// Authorisation
	@Override
	protected void configure(HttpSecurity http) throws Exception {
		// disable csrf(Cross-Site Request Forgery)
		http.csrf().disable();
		
		// authorisation
		http.authorizeRequests().antMatchers("/test/user").hasRole("USER");
		http.authorizeRequests().antMatchers("/test/moderator").hasRole("MODERATOR");
		http.authorizeRequests().antMatchers("/test/admin").hasRole("ADMIN");
		
		// authenticate all the requests
		http.authorizeRequests().anyRequest().authenticated();
		
		// enable basic authentication
		http.httpBasic();
	}
	
}
