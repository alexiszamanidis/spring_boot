-- insert users
insert into user(id, user_name, email, password, creation_date) values (1, "alexzam", "alexzam@gmail.com", "password", CURRENT_DATE());
insert into user(id, user_name, email, password, creation_date) values (2, "johndar", "johndar@gmail.com", "password", CURRENT_DATE());
insert into user(id, user_name, email, password, creation_date) values (3, "nickntaf", "nickntaf@gmail.com", "password", CURRENT_DATE());
insert into user(id, user_name, email, password, creation_date) values (4, "vagdem", "vagdem@gmail.com", "password", CURRENT_DATE());
insert into user(id, user_name, email, password, creation_date) values (5, "stathdem", "stathdem@gmail.com", "password", CURRENT_DATE());

-- insert roles
insert into role(id, name) values (1, "ROLE_USER");
insert into role(id, name) values (2, "ROLE_ADMIN");
insert into role(id, name) values (3, "ROLE_MODERATOR");

-- insert authorities
insert into authority(id, name) values (1, "AUTHORITY_READ");
insert into authority(id, name) values (2, "AUTHORITY_WRITE");
insert into authority(id, name) values (3, "AUTHORITY_UPDATE");
insert into authority(id, name) values (4, "AUTHORITY_DELETE");

-- insert user-role
insert into user_role(user_id, role_id) values (1, 1);
insert into user_role(user_id, role_id) values (1, 2);
insert into user_role(user_id, role_id) values (1, 3);

insert into user_role(user_id, role_id) values (2, 1);
insert into user_role(user_id, role_id) values (2, 3);

insert into user_role(user_id, role_id) values (3, 1);
insert into user_role(user_id, role_id) values (3, 3);

insert into user_role(user_id, role_id) values (4, 1);
insert into user_role(user_id, role_id) values (4, 3);

insert into user_role(user_id, role_id) values (5, 2);

-- insert user-authority
insert into user_authority(user_id, authority_id) values (1, 1);
insert into user_authority(user_id, authority_id) values (1, 2);
insert into user_authority(user_id, authority_id) values (1, 3);
insert into user_authority(user_id, authority_id) values (1, 4);

insert into user_authority(user_id, authority_id) values (2, 1);
insert into user_authority(user_id, authority_id) values (2, 2);
insert into user_authority(user_id, authority_id) values (2, 3);

insert into user_authority(user_id, authority_id) values (3, 1);
insert into user_authority(user_id, authority_id) values (3, 2);
insert into user_authority(user_id, authority_id) values (3, 3);

insert into user_authority(user_id, authority_id) values (4, 1);
insert into user_authority(user_id, authority_id) values (4, 2);
insert into user_authority(user_id, authority_id) values (4, 3);

insert into user_authority(user_id, authority_id) values (5, 1);

