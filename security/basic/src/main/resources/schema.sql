-- drop user table and recreate it
drop table if exists user;

create table user(
	id bigint not null AUTO_INCREMENT primary key,
	user_name varchar(50) not null,
	email varchar(50) not null,
	password varchar(50) not null,
	creation_date date not null
);

-- drop role table and recreate it
drop table if exists role;

create table role(
	id int not null primary key,
	name varchar(50) not null
);

-- drop authority table and recreate it
drop table if exists authority;

create table authority(
	id int not null primary key,
	name varchar(50) not null
);

-- drop user_role table and recreate it
drop table if exists user_role;

create table user_role(
	user_id int not null,
	role_id int not null
);

-- drop user_authority table and recreate it
drop table if exists user_authority;

create table user_authority(
	user_id int not null,
	authority_id int not null
);
