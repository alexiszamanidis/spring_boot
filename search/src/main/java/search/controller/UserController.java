package search.controller;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import search.model.User;
import search.service.UserService;

@RestController
@RequestMapping(value = "/users")
public class UserController {

	@Autowired
	public UserService userService;
	
	@GetMapping(value = "/filter_search")
	public ResponseEntity<List<User>> filter_search(@RequestParam Optional<String> keyword) {
		return new ResponseEntity<>(userService.filter_search(keyword), HttpStatus.OK);
	}
	
	@GetMapping(value = "/full_text_search")
	public ResponseEntity<List<User>> full_text_search(@RequestParam Optional<String> keyword) {
		return new ResponseEntity<>(userService.full_text_search(keyword), HttpStatus.OK);
	}
	
}
