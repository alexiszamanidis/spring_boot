package search.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import search.model.User;

@Repository
public interface UserRepository extends JpaRepository<User, Integer> {
//  check each column separately
	@Query("select u from User u where firstName like %?1% "
					             + "or lastName like %?1% "
					             + "or email like %?1%"
					             + "or creationDate like %?1%")
//  spaces between concatenated columns
//	@Query("select u from User u where concat(u.firstName, ' ', u.lastName, ' ', u.email, ' ', u.creationDate) like %?1%")
//  no-spaces between concatenated columns
//	@Query("select u from User u where concat(u.firstName, u.lastName, u.email, u.creationDate) like %?1%")
	public List<User> filter_search(String keyword);
	
	@Query(value = "select * from user where match(first_name, last_name, email) against(?1)", nativeQuery = true)
	public List<User> full_text_search(String keyword);
	
}
