package search.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import search.model.User;
import search.repository.UserRepository;

@Service
public class UserService {

	@Autowired
	public UserRepository userRepository;
	
	public List<User> filter_search(Optional<String> keyword) {
		String getKeyword = keyword.orElse(null);
		System.out.println(getKeyword);
		if(getKeyword == null) {
			return userRepository.findAll();
		}
		else {
			return userRepository.filter_search(getKeyword);
		}
	}
	
	public List<User> full_text_search(Optional<String> keyword) {
		String getKeyword = keyword.orElse(null);
		if(getKeyword == null) {
			return userRepository.findAll();
		}
		else {
			return userRepository.full_text_search(getKeyword);
		}
	}
	
}
