

-- insert users
insert into user(id, first_name, last_name, email, creation_date) values (1, "Alexis", "Zamanidis", "alexiszamanidis@hotmail.com", CURRENT_DATE());
insert into user(id, first_name, last_name, email, creation_date) values (2, "Nikos", "Ntaflos", "nikontaflos@hotmail.com", CURRENT_DATE());
insert into user(id, first_name, last_name, email, creation_date) values (3, "Giannis", "Darzentas", "giannisdarzentas@hotmail.com", CURRENT_DATE());
insert into user(id, first_name, last_name, email, creation_date) values (4, "Vaggelis", "Demenagas", "vaggellisdemenagas@hotmail.com", CURRENT_DATE());
insert into user(id, first_name, last_name, email, creation_date) values (5, "Stathis", "Demenagas", "stathisdemenagas@hotmail.com", CURRENT_DATE());
insert into user(id, first_name, last_name, email, creation_date) values (6, "John", "Snow", "johnsnow@hotmail.com", CURRENT_DATE());
insert into user(id, first_name, last_name, email, creation_date) values (7, "Bean", "Kills", "beankills@hotmail.com", CURRENT_DATE());
insert into user(id, first_name, last_name, email, creation_date) values (8, "Kappa", "Keepo", "kappakeepo@hotmail.com", CURRENT_DATE());
insert into user(id, first_name, last_name, email, creation_date) values (9, "Jenny", "Demenaga", "jennydemenaga@hotmail.com", CURRENT_DATE());
insert into user(id, first_name, last_name, email, creation_date) values (10, "Liza", "Zamanidi", "lizazamanidi@hotmail.com", CURRENT_DATE());
insert into user(id, first_name, last_name, email, creation_date) values (11, "Faraw", "Kritharidis", "farawkritharidis@hotmail.com", CURRENT_DATE());
insert into user(id, first_name, last_name, email, creation_date) values (12, "Panos", "Serem", "panosserem@hotmail.com", CURRENT_DATE());
insert into user(id, first_name, last_name, email, creation_date) values (13, "Nikos", "Vasilev", "nikosvasilev@hotmail.com", CURRENT_DATE());
insert into user(id, first_name, last_name, email, creation_date) values (14, "Pog", "Champ", "pogchamp@hotmail.com", CURRENT_DATE());
insert into user(id, first_name, last_name, email, creation_date) values (15, "Eminem", "Rapper", "eminemrapper@hotmail.com", CURRENT_DATE());
insert into user(id, first_name, last_name, email, creation_date) values (16, "Drake", "Rapper", "drakerapper@hotmail.com", CURRENT_DATE());
insert into user(id, first_name, last_name, email, creation_date) values (17, "Mad", "Clip", "madclip@hotmail.com", CURRENT_DATE());
insert into user(id, first_name, last_name, email, creation_date) values (18, "Light", "Capital", "lightcapital@hotmail.com", CURRENT_DATE());
insert into user(id, first_name, last_name, email, creation_date) values (19, "Snik", "Hustla", "snikhustla@hotmail.com", CURRENT_DATE());
insert into user(id, first_name, last_name, email, creation_date) values (20, "Mente", "Toumpas", "mentetoumpas@hotmail.com", CURRENT_DATE());

