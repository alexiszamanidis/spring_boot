


-- drop user table and recreate it
drop table if exists user;

create table user(
	id int not null AUTO_INCREMENT primary key,
	first_name varchar(200) not null,
	last_name varchar(200) not null,
	email varchar(200) not null,
	creation_date date not null
);
