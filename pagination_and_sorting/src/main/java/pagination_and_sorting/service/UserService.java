package pagination_and_sorting.service;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestParam;

import pagination_and_sorting.model.User;
import pagination_and_sorting.repository.UserRepository;

@Service
public class UserService {

	@Autowired
	private UserRepository userRepository;
	
	public Page<User> getAllUsers(@RequestParam Optional<Integer> page,
								  @RequestParam Optional<Integer> elementsPerPage,
								  @RequestParam Optional<String> sortField,
								  @RequestParam Optional<String> sortDirection) {
		int defaultPage = 0, defaultElementsPerPage = 8;
		String getSortField = sortField.orElse("id");
		Sort getSortDirection;
		if(sortDirection.orElse("ascending").equals("ascending")) {
			getSortDirection = Sort.by(getSortField).ascending();
		}
		else {
			getSortDirection = Sort.by(getSortField).descending();
		}
		
		Pageable pagging = PageRequest.of(page.orElse(defaultPage), elementsPerPage.orElse(defaultElementsPerPage), getSortDirection);
		return (Page<User>) userRepository.findAll(pagging);
	}

	
}
