package pagination_and_sorting.controller;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import pagination_and_sorting.model.User;
import pagination_and_sorting.service.UserService;

@RestController
@RequestMapping(value = "/users")
public class UserController {

	@Autowired
	private UserService userService;
	
	@RequestMapping(method = RequestMethod.GET)
	public ResponseEntity<Page<User>> getAllUsers(@RequestParam Optional<Integer> page,
			 									  @RequestParam Optional<Integer> elementsPerPage,
			 									  @RequestParam Optional<String> sortField,
			 									  @RequestParam Optional<String> sortDirection) {
		return new ResponseEntity<>(userService.getAllUsers(page, elementsPerPage, sortField, sortDirection), HttpStatus.OK);
	}

}
