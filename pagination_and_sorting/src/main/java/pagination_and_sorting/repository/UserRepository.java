package pagination_and_sorting.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import pagination_and_sorting.model.User;

@Repository
public interface UserRepository extends JpaRepository<User, Integer> {

	Page<User> findAll(Pageable pageable);
	
}
