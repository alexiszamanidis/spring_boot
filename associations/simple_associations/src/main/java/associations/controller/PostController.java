package associations.controller;

import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import associations.exception.ErrorServiceException;
import associations.model.Post;
import associations.model.Tag;
import associations.repository.TagRepository;
import associations.service.PostService;

@RestController
@RequestMapping(value = "/posts")
public class PostController {

	@Autowired
	private PostService postService;
	
	@Autowired
	private TagRepository tagRepository;
	
	@GetMapping
	public ResponseEntity<List<Post>> getAllUsers() {
		return new ResponseEntity<>(postService.getAllUsers(), HttpStatus.OK);
	}
	
	@PostMapping
	public ResponseEntity<Post> savePost(@RequestBody Post postDetails) throws ErrorServiceException {
		// create a Set of tags using the tag id List from postDetails
	    Set<Tag> tags = postDetails.getTags().stream()
	    		                    .map(t -> {
	    		                    			Tag tag = tagRepository.findById(t.getId()).get();
	    		                    			return tag;
	    		                    }).collect(Collectors.toSet());
	    // create new post object
	    Post newPost = new Post(postDetails.getTitle(), postDetails.getDetails(), postDetails.getUserId(), tags);
	    newPost.getTags().addAll(tags);
	    
	    return new ResponseEntity<>(postService.savePost(newPost), HttpStatus.CREATED);
	}
	
	@GetMapping(value = "/{id}")
	public ResponseEntity<Post> getPost(@PathVariable Integer id) throws ErrorServiceException {
		return new ResponseEntity<>(postService.getPost(id), HttpStatus.OK);
	}

	@DeleteMapping(value = "/{id}")
	public ResponseEntity<Object> deletePost(@PathVariable Integer id) throws ErrorServiceException {
		postService.deletePost(id);
		return new ResponseEntity<>(HttpStatus.OK);
	}
	
	@PutMapping(value = "/{id}")
	public ResponseEntity<Post> updatePost(@PathVariable Integer id, @RequestBody Post postDetails) throws ErrorServiceException {
		return new ResponseEntity<>(postService.updatePost(id, postDetails), HttpStatus.OK);
	}
	
}
