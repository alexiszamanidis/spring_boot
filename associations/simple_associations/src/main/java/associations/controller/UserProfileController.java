package associations.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import associations.exception.ErrorServiceException;
import associations.model.UserProfile;
import associations.service.UserProfileService;

@RestController
@RequestMapping(value = "/user_profiles")
public class UserProfileController {
	
	@Autowired
	private UserProfileService userProfileService;

	@GetMapping
	public ResponseEntity<List<UserProfile>> getAllUsers() {
		return new ResponseEntity<>(userProfileService.getAllUserProfiles(), HttpStatus.OK);
	}

	@PostMapping
	public ResponseEntity<UserProfile> saveUser(@RequestBody UserProfile userProfileDetails) throws ErrorServiceException {
		UserProfile newUserProfile = new UserProfile(userProfileDetails.getPhoneNumber(), userProfileDetails.getAddress(), userProfileDetails.getGender(), userProfileDetails.getUserId());
		return new ResponseEntity<>(userProfileService.saveUserProfile(newUserProfile), HttpStatus.CREATED);
	}
	
	@GetMapping(value = "/{id}")
	public ResponseEntity<UserProfile> getUser(@PathVariable Integer id) throws ErrorServiceException {
		return new ResponseEntity<>(userProfileService.getUserProfile(id), HttpStatus.OK);
	}

	@DeleteMapping(value = "/{id}")
	public ResponseEntity<Object> deleteUser(@PathVariable Integer id) throws ErrorServiceException {
		userProfileService.deleteUserProfile(id);
		return new ResponseEntity<>(HttpStatus.OK);
	}
	
	@PutMapping(value = "/{id}")
	public ResponseEntity<UserProfile> updateUser(@PathVariable Integer id, @RequestBody UserProfile userProfileDetails) throws ErrorServiceException {
		return new ResponseEntity<>(userProfileService.updateUserProfile(id, userProfileDetails), HttpStatus.OK);
	}
	
}
