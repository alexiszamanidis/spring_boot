package associations.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import associations.exception.ErrorServiceException;
import associations.model.Tag;
import associations.service.TagService;

@RestController
@RequestMapping(value = "/tags")
public class TagController {

	@Autowired
	private TagService tagService;
	
	@GetMapping
	public ResponseEntity<List<Tag>> getAllTags() {
		return new ResponseEntity<>(tagService.getAllTags(), HttpStatus.OK);
	}
	
	@PostMapping
	public ResponseEntity<Tag> saveTag(@RequestBody Tag tagDetails) {
	    Tag newTag = new Tag(tagDetails.getName());
		return new ResponseEntity<>(tagService.saveTag(newTag), HttpStatus.CREATED);
	}
	
	@GetMapping(value = "/{id}")
	public ResponseEntity<Tag> getTag(@PathVariable Integer id) throws ErrorServiceException {
		return new ResponseEntity<>(tagService.getTag(id), HttpStatus.OK);
	}

	@DeleteMapping(value = "/{id}")
	public ResponseEntity<Object> deleteTag(@PathVariable Integer id) throws ErrorServiceException {
		tagService.deleteTag(id);
		return new ResponseEntity<>(HttpStatus.OK);
	}
	
	@PutMapping(value = "/{id}")
	public ResponseEntity<Tag> updateTag(@PathVariable Integer id, @RequestBody Tag tagDetails) throws ErrorServiceException {
		return new ResponseEntity<>(tagService.updateTag(id, tagDetails), HttpStatus.OK);
	}
	
}
