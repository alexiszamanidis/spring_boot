package associations.model;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;

public class Error {
	
	private int statusCode;
	
	private String errorMessage;
	
	@JsonFormat(pattern="yyyy-MM-dd")
	private Date errorDate;
	
	
	public Error() {
		
	}
	
	public Error(int statusCode, String errorMessage, Date errorDate) {
		super();
		this.statusCode = statusCode;
		this.errorMessage = errorMessage;
		this.errorDate = errorDate;
	}

	public int getStatusCode() {
		return statusCode;
	}

	public void setStatusCode(int statusCode) {
		this.statusCode = statusCode;
	}

	public String getErrorMessage() {
		return errorMessage;
	}

	public void setErrorMessage(String errorMessage) {
		this.errorMessage = errorMessage;
	}

	public Date getErrorDate() {
		return errorDate;
	}

	public void setErrorDate(Date errorDate) {
		this.errorDate = errorDate;
	}

}
