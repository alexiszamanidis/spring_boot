package associations.model;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonFormat;

@Entity
@Table(name = "post")
public class Post {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private int id;
	
	@Column(name = "title")
	private String title;
	
	@Column(name = "details")
	private String details;

	@Column(name = "creation_date")
	@JsonFormat(pattern="yyyy-MM-dd")
	private Date creationDate = new Date();

	@ManyToOne
	@JoinColumn(name = "user_id", referencedColumnName = "id", insertable = false, updatable = false)
	private User user;
	@Column(name = "user_id")
	private int userId;
	
	@ManyToMany(fetch = FetchType.LAZY)
	@JoinTable(name = "post_tags",
			   joinColumns = { @JoinColumn(name = "post_id")},
			   inverseJoinColumns = { @JoinColumn(name = "tag_id")})
	private Set<Tag> tags = new HashSet<Tag>();
	
	public Post() {
		
	}
	
	public Post(String title, String details, Integer userId) {
		this.title = title;
		this.details = details;
		this.userId = userId;
	}
	
	public Post(String title, String details, Integer userId, Set<Tag> tags) {
		this.title = title;
		this.details = details;
		this.userId = userId;
		this.tags = tags;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getDetails() {
		return details;
	}

	public void setDetails(String details) {
		this.details = details;
	}

	public Date getCreationDate() {
		return creationDate;
	}

	public void setCreationDate(Date creationDate) {
		this.creationDate = creationDate;
	}
	
	@JsonBackReference
	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}
	
	public int getUserId() {
		return userId;
	}

	public void setUserId(int userId) {
		this.userId = userId;
	}

	public Set<Tag> getTags() {
		return tags;
	}

	public void setTags(Set<Tag> tags) {
		this.tags = tags;
	}

}
