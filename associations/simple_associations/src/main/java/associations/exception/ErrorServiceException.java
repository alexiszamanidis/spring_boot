package associations.exception;

public class ErrorServiceException extends Exception {
	
	private static final long serialVersionUID = 1L;

	public ErrorServiceException(String message) {
		super(message);
	}

}
