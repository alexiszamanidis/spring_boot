package associations.exception;

import java.util.Date;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import associations.model.Error;

@ControllerAdvice
public class ErrorExceptionAdvice {
	
	@ExceptionHandler(ErrorServiceException.class)
	public ResponseEntity<Error> notFoundException(ErrorServiceException ex) {
		Error error = new Error(HttpStatus.NOT_FOUND.value(), ex.getMessage(), new Date());
		return new ResponseEntity<Error>(error, HttpStatus.NOT_FOUND);
	}
}