package associations.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import associations.exception.ErrorServiceException;
import associations.model.Tag;
import associations.repository.TagRepository;

@Service
public class TagService {

	@Autowired
	private TagRepository tagRepository;
	
	
	public List<Tag> getAllTags() {
		return (List<Tag>) tagRepository.findAll();
	}
	
	public Tag saveTag(Tag tag) {
		return tagRepository.save(tag);
	}
	
	public Tag getTag(Integer id) throws ErrorServiceException {
		try {
			Tag tag = tagRepository.findById(id).get();
			
			return tag;
		}
		catch( Exception e ) {
			throw new ErrorServiceException("TagService.java: getTag(): Tag does not exist with id: " + id);
		}
	}
	
	public void deleteTag(Integer id) throws ErrorServiceException {
		try {
			tagRepository.deleteById(id);
		}
		catch( Exception e ) {
			throw new ErrorServiceException("TagService.java: deleteTag(): Tag does not exist with id: " + id);
		}
	}
	
	public Tag updateTag(Integer id, Tag tagDetails) throws ErrorServiceException {
		try {
			Tag tag = tagRepository.findById(id).get();
			
			tag.setName(tagDetails.getName());
			
			Tag updatedTag = tagRepository.save(tag);
			
			return updatedTag;
		}
		catch( Exception e ) {
			throw new ErrorServiceException("TagService.java: updateTag(): Tag does not exist with id: " + id);
		}
	}
	
}
