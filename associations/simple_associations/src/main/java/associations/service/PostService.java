package associations.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import associations.exception.ErrorServiceException;
import associations.model.Post;
import associations.repository.PostRepository;
import associations.repository.UserRepository;

@Service
public class PostService {

	@Autowired
	private PostRepository postRepository;
	
	@Autowired
	private UserRepository userRepository;
	
	public List<Post> getAllUsers() {
		return (List<Post>) postRepository.findAll();
	}
	
	public Post savePost(Post post) throws ErrorServiceException {
		try {
			// check if the user exists
			userRepository.findById(post.getUserId()).get();
			
			return postRepository.save(post);
		}
		catch( Exception e ) {
			throw new ErrorServiceException("PostService.java: savePost(): User does not exist with id: " + post.getUserId());
		}
	}
	
	public Post getPost(Integer id) throws ErrorServiceException {
		try {
			Post post = postRepository.findById(id).get();
			
			return post;
		}
		catch( Exception e ) {
			throw new ErrorServiceException("PostService.java: getPost(): Post does not exist with id: " + id);
		}
	}
	
	public void deletePost(Integer id) throws ErrorServiceException {
		try {
			postRepository.deleteById(id);
		}
		catch( Exception e ) {
			throw new ErrorServiceException("PostService.java: deletePost(): Post does not exist with id: " + id);
		}
	}
	
	public Post updatePost(Integer id, Post postDetails) throws ErrorServiceException {
		try {
			Post post = postRepository.findById(id).get();
			
			post.setTitle(postDetails.getTitle());
			post.setDetails(postDetails.getDetails());
			
			Post updatedPost = postRepository.save(post);
			
			return updatedPost;
		}
		catch( Exception e ) {
			throw new ErrorServiceException("PostService.java: updatePost(): Post does not exist with id: " + id);
		}
	}
	
}
