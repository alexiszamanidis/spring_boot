package associations.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import associations.exception.ErrorServiceException;
import associations.model.User;
import associations.repository.UserRepository;

@Service
public class UserService {

	@Autowired
	private UserRepository userRepository;
	
	public List<User> getAllUsers() {
		return (List<User>) userRepository.findAll();
	}
	
	public User saveUser(User user) {
		return userRepository.save(user);
	}
	
	public User getUser(Integer id) throws ErrorServiceException {
		try {
			User user = userRepository.findById(id).get();
			
			return user;
		}
		catch( Exception e ) {
			throw new ErrorServiceException("UserService.java: getUser(): User does not exist with id: " + id);
		}
	}

	public void deleteUser(Integer id) throws ErrorServiceException {
		try {
			userRepository.deleteById(id);
		}
		catch( Exception e ) {
			throw new ErrorServiceException("UserService.java: deleteUser(): User does not exist with id: " + id);
		}
	}
	
	public User updateUser(Integer id, User userDetails) throws ErrorServiceException {
		try {
			User user = userRepository.findById(id).get();
			
			user.setFirstName(userDetails.getFirstName());
			user.setLastName(userDetails.getLastName());
			user.setEmail(userDetails.getEmail());
			
			User updatedUser = userRepository.save(user);
			
			return updatedUser;
		}
		catch( Exception e ) {
			throw new ErrorServiceException("UserService.java: updateUser(): User does not exist with id: " + id);
		}
	}

}
