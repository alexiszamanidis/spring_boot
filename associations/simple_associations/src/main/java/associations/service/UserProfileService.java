package associations.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import associations.exception.ErrorServiceException;
import associations.model.UserProfile;
import associations.repository.UserProfileRepository;
import associations.repository.UserRepository;

@Service
public class UserProfileService {

	@Autowired
	private UserProfileRepository userProfileRepository;
	
	@Autowired
	private UserRepository userRepository;
	
	public List<UserProfile> getAllUserProfiles() {
		return (List<UserProfile>) userProfileRepository.findAll();
	}
	
	public UserProfile saveUserProfile(UserProfile userProfile) throws ErrorServiceException {
		try {
			// check if the user exists
			userRepository.findById(userProfile.getUserId()).get();
			
			return userProfileRepository.save(userProfile);
		}
		catch( Exception e ) {
			throw new ErrorServiceException("UserProfileService.java: saveUserProfile(): User does not exist with id: " + userProfile.getUserId());
		}
	}
	
	public UserProfile getUserProfile(Integer id) throws ErrorServiceException {
		try {
			UserProfile userProfile = userProfileRepository.findById(id).get();
			
			return userProfile;
		}
		catch( Exception e ) {
			throw new ErrorServiceException("UserProfileService.java: getUserProfile(): UserProfile does not exist with id: " + id);
		}
	}
	
	public void deleteUserProfile(Integer id) throws ErrorServiceException {
		try {
			userProfileRepository.deleteById(id);
		}
		catch( Exception e ) {
			throw new ErrorServiceException("UserProfileService.java: deleteUserProfile(): UserProfile does not exist with id: " + id);
		}
	}
	
	public UserProfile updateUserProfile(Integer id, UserProfile userProfileDetails) throws ErrorServiceException {
		try {
			UserProfile userProfile = userProfileRepository.findById(id).get();
			
			userProfile.setPhoneNumber(userProfileDetails.getPhoneNumber());
			userProfile.setAddress(userProfileDetails.getAddress());
			userProfile.setGender(userProfileDetails.getGender());
			
			UserProfile updatedUserProfile = userProfileRepository.save(userProfile);
			
			return updatedUserProfile;
		}
		catch( Exception e ) {
			throw new ErrorServiceException("UserProfileService.java: updateUserProfile(): UserProfile does not exist with id: " + id);
		}
	}
	
}
