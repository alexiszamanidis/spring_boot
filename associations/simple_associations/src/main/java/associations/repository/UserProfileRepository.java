package associations.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import associations.model.UserProfile;

public interface UserProfileRepository extends JpaRepository<UserProfile, Integer> {

}
