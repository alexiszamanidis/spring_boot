package associations.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import associations.model.Post;

public interface PostRepository extends JpaRepository<Post, Integer> {

}
