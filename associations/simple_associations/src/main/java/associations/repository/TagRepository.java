package associations.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import associations.model.Tag;

public interface TagRepository extends JpaRepository<Tag, Integer> {

}
