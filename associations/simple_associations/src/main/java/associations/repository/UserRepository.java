package associations.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import associations.model.User;

@Repository
public interface UserRepository extends JpaRepository<User, Integer> {

}
