


-- insert users
insert into user(id, first_name, last_name, email, creation_date, user_profile_id) values (1, "Alexis", "Zamanidis", "alexiszamanidis@hotmail.com", CURRENT_DATE(), 1);
insert into user(id, first_name, last_name, email, creation_date, user_profile_id) values (2, "Nikos", "Ntaflos", "nikontaflos@hotmail.com", CURRENT_DATE(), 2);
insert into user(id, first_name, last_name, email, creation_date, user_profile_id) values (3, "Giannis", "Darzentas", "giannisdarzentas@hotmail.com", CURRENT_DATE(), 3);
insert into user(id, first_name, last_name, email, creation_date, user_profile_id) values (4, "Vaggelis", "Demenagas", "vaggellisdemenagas@hotmail.com", CURRENT_DATE(), 4);
insert into user(id, first_name, last_name, email, creation_date, user_profile_id) values (5, "Stathis", "Demenagas", "stathisdemenagas@hotmail.com", CURRENT_DATE(), 5);



-- insert posts
insert into post(id, title, details, creation_date, user_id) values (1, "First Post Title", "First Post Details", CURRENT_DATE(), 1);
insert into post(id, title, details, creation_date, user_id) values (2, "Second Post Title", "Second Post Details", CURRENT_DATE(), 1);
insert into post(id, title, details, creation_date, user_id) values (3, "Third Post Title", "Third Post Details", CURRENT_DATE(), 1);

insert into post(id, title, details, creation_date, user_id) values (4, "First Post Title", "First Post Details", CURRENT_DATE(), 2);
insert into post(id, title, details, creation_date, user_id) values (5, "Second Post Title", "Second Post Details", CURRENT_DATE(), 2);
insert into post(id, title, details, creation_date, user_id) values (6, "Third Post Title", "Third Post Details", CURRENT_DATE(), 2);

insert into post(id, title, details, creation_date, user_id) values (7, "First Post Title", "First Post Details", CURRENT_DATE(), 3);
insert into post(id, title, details, creation_date, user_id) values (8, "Second Post Title", "Second Post Details", CURRENT_DATE(), 3);
insert into post(id, title, details, creation_date, user_id) values (9, "Third Post Title", "Third Post Details", CURRENT_DATE(), 3);

insert into post(id, title, details, creation_date, user_id) values (10, "First Post Title", "First Post Details", CURRENT_DATE(), 4);
insert into post(id, title, details, creation_date, user_id) values (11, "Second Post Title", "Second Post Details", CURRENT_DATE(), 4);
insert into post(id, title, details, creation_date, user_id) values (12, "Third Post Title", "Third Post Details", CURRENT_DATE(), 4);

insert into post(id, title, details, creation_date, user_id) values (13, "First Post Title", "First Post Details", CURRENT_DATE(), 5);
insert into post(id, title, details, creation_date, user_id) values (14, "Second Post Title", "Second Post Details", CURRENT_DATE(), 5);
insert into post(id, title, details, creation_date, user_id) values (15, "Third Post Title", "Third Post Details", CURRENT_DATE(), 5);



-- insert tags
insert into tag(id, name) values (1, "First Tag");
insert into tag(id, name) values (2, "Second Tag");
insert into tag(id, name) values (3, "Third Tag");
insert into tag(id, name) values (4, "Fourth Tag");
insert into tag(id, name) values (5, "Fifth Tag");



-- insert post tags
insert into post_tags(post_id, tag_id) values (1, 1);
insert into post_tags(post_id, tag_id) values (1, 2);
insert into post_tags(post_id, tag_id) values (1, 3);
insert into post_tags(post_id, tag_id) values (1, 4);
insert into post_tags(post_id, tag_id) values (1, 5);

insert into post_tags(post_id, tag_id) values (2, 1);
insert into post_tags(post_id, tag_id) values (2, 2);
insert into post_tags(post_id, tag_id) values (2, 3);
insert into post_tags(post_id, tag_id) values (2, 4);
insert into post_tags(post_id, tag_id) values (2, 5);

insert into post_tags(post_id, tag_id) values (3, 1);
insert into post_tags(post_id, tag_id) values (3, 2);
insert into post_tags(post_id, tag_id) values (3, 3);
insert into post_tags(post_id, tag_id) values (3, 4);
insert into post_tags(post_id, tag_id) values (3, 5);

insert into post_tags(post_id, tag_id) values (4, 1);
insert into post_tags(post_id, tag_id) values (4, 2);
insert into post_tags(post_id, tag_id) values (4, 3);
insert into post_tags(post_id, tag_id) values (4, 4);
insert into post_tags(post_id, tag_id) values (4, 5);

insert into post_tags(post_id, tag_id) values (5, 1);
insert into post_tags(post_id, tag_id) values (5, 2);
insert into post_tags(post_id, tag_id) values (5, 3);
insert into post_tags(post_id, tag_id) values (5, 4);
insert into post_tags(post_id, tag_id) values (5, 5);

insert into post_tags(post_id, tag_id) values (6, 1);
insert into post_tags(post_id, tag_id) values (6, 2);
insert into post_tags(post_id, tag_id) values (6, 3);
insert into post_tags(post_id, tag_id) values (6, 4);
insert into post_tags(post_id, tag_id) values (6, 5);

insert into post_tags(post_id, tag_id) values (1, 1);
insert into post_tags(post_id, tag_id) values (1, 2);
insert into post_tags(post_id, tag_id) values (1, 3);
insert into post_tags(post_id, tag_id) values (1, 4);
insert into post_tags(post_id, tag_id) values (1, 5);

insert into post_tags(post_id, tag_id) values (2, 1);
insert into post_tags(post_id, tag_id) values (2, 2);
insert into post_tags(post_id, tag_id) values (2, 3);
insert into post_tags(post_id, tag_id) values (2, 4);
insert into post_tags(post_id, tag_id) values (2, 5);

insert into post_tags(post_id, tag_id) values (3, 1);
insert into post_tags(post_id, tag_id) values (3, 2);
insert into post_tags(post_id, tag_id) values (3, 3);
insert into post_tags(post_id, tag_id) values (3, 4);
insert into post_tags(post_id, tag_id) values (3, 5);



-- insert user profiles
insert into user_profile(id, phone_number, address, gender, user_id) values (1, "6969696969", "Praxitelous 1", "MALE", 1);
insert into user_profile(id, phone_number, address, gender, user_id) values (2, "6969696969", "Praxitelous 2", "MALE", 2);
insert into user_profile(id, phone_number, address, gender, user_id) values (3, "6969696969", "Praxitelous 3", "MALE", 3);
insert into user_profile(id, phone_number, address, gender, user_id) values (4, "6969696969", "Praxitelous 4", "MALE", 4);
insert into user_profile(id, phone_number, address, gender, user_id) values (5, "6969696969", "Praxitelous 5", "MALE", 5);






