


-- drop user table and recreate it
drop table if exists user;

create table user(
	id int not null AUTO_INCREMENT primary key,
	first_name varchar(200) not null,
	last_name varchar(200) not null,
	email varchar(200) not null,
	creation_date date not null,
	user_profile_id int not null
);



-- drop post table and recreate it
drop table if exists post;

create table post(
	id int not null AUTO_INCREMENT primary key,
	title varchar(200) not null,
	details varchar(400) not null,
	creation_date date not null,
	user_id int not null
);



-- drop tag table and recreate it
drop table if exists tag;

create table tag(
	id int not null AUTO_INCREMENT primary key,
	name varchar(200) not null
);



-- drop posts_tags table and recreate it
drop table if exists post_tags;

create table post_tags(
	post_id int not null,
	tag_id varchar(200) not null
);



-- drop user_profile table and recreate it
drop table if exists user_profile;

create table user_profile(
	id int not null AUTO_INCREMENT primary key,
	phone_number varchar(200) DEFAULT null,
	address varchar(200) DEFAULT null,
	gender varchar(200) DEFAULT null,
	user_id int not null
);







