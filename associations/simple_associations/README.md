## Associations

Learning about associations(@OneToOne, @OneToMany, @ManyToOne, @ManyToMany) in Spring Boot.

#### Spring boot Class diagram

![spring_boot_association](https://user-images.githubusercontent.com/48658768/95688696-ab6f2800-0c14-11eb-894c-898f692eb976.png)

#### Database diagram from SQL server

![my_sql_association](https://user-images.githubusercontent.com/48658768/95688695-aa3dfb00-0c14-11eb-9c67-0c2841c5b632.png)

#### Dependencies

* Spring Web
* MySQL Driver
* Spring Data JPA